# Gulp Boilerplate

**Version 2.2.0**

![VersionEye](https://www.versioneye.com/user/projects/55c283676537620017002ad8/badge.svg?style=flat)

The Gulp Boilerplate is a starting point for frontend development using [Gulp](http://gulpjs.com/). It contains several tasks to make your life easier, like CSS preprocessing, JS bundling, image optimization and more.

## Installation

* Place the files from this repository in a new directory named `boilerplate`* in your project's root directory.
* `cd` into that directory and run `npm install`.
* Copy the `project.json.dist` file to your project's root directory (you can use `cp project.json.dist ../project.json` if you're still in the `boilerplate` directory).
* Create a new directory called `resources`* in your project's root directory. This will contain the source files of your CSS, JS, images, fonts and all the other stuff you need. We recommend the following directory structure:

```
<project root>
├ boilerplate/
│ └ …
├ resources/
│ ├ fonts/
│ ├ less/
│ ├ sass/
│ ├ img/
│ └ js/
└ project.json
```

* Check the `paths` configuration in `project.json`. Make sure that you have at least the following path aliases set up:
    * `root`: The boilerplate's root directory (keep this at `.`).
    * `theme`: This is the path alias for your `resources` directory.
    * `build`: Everything will be built to this directory.
    * `content`: Set this to where user uploaded images are stored.
    * `bower`: Set this to whatever you configured in `boilerplate/.bowerrc`. Please set up this alias regardless of whether you're planning to use Bower or not.
* *(Optional)* Grab the livereload extension for Google Chrome [at the Chrome Web Store](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en) 
* All that's left to do now is running `gulp` inside the `boilerplate` directory. 

—

(*) As with most recommendations in this document, the folder names are based on our experience and workflows. Feel free to adapt.

### Requirements

* NPM
* Gulp installed globally

## Notes

* The `css` and `js` tasks are dynamically starting gulp tasks (`less`, `sass` and `scripts`, `browserify` respectively) depending on your settings in `project.json`, in case you're wondering.
* We are planning to add support for Coffeescript, Typescript and more in the future.
* This is tested only under Mac OS X. It should work fine on other Unix-like systems, Windows users will have to fight for themselves.

---

## Usage/`project.json` Reference

The `project.json` file is where you configure several task parameters as well as several other settings regarding build processes and file paths. This _should_ be the only file you have to edit to get things up and running.

You can find the JSON Schema (draft 4) compliant schema for `project.json` in `gulp/support/project-schema.json`. It is validated prior to every gulp task execution and will report errors in your `project.json` file, if applicable.

**Note**: The schema is a major work in progress and may not work properly. You can disable it by commenting out line 11 of `gulpfile.js`:

```js
// Comment this out to disable the validator
schemaValidator = requireReload(…),
```

### `title` (string)

The name of your project. This is mainly for internal reference purposes.

### `verbose` (boolean) – Console Verbosity

This setting will output additional build step information when set to `true`. When set to `false`, you'll see only the standard gulp output and only the most important messages.

### `paths` (object) – Path Aliases

* You can set path aliases under the `path` key in `project.json`. Those are just shortcuts to some folders. Please leave `root`, `theme`, `build`, `bower` and `content` as they are. Everything's already set up. 
* **Please note that paths in** `project.json` **are always relative to the** `boilerplate` **directory, except if you're using path aliases (which are relative to the** `boilerplate` **directory as well).**  So, if you define a path without an alias in it somewhere in `project.json`, you'll have to enter it's location relative to the `boilerplate` directory, for example `../README.md` for `README.md` (the one in the project's root folder — this one).
* The `paths` object basically contains key/value pairs of path aliases and paths. There are some default path aliases configured, we strongly recommend to leave these in there. You can change the paths, obviously, just not the aliases.

### `css` (object) – Preprocessor Configuration

* The `preprocessor` option can be `less`, or `sass`. Their purpose is self-explanatory.
* `minify` controls minification as well as source map generation. Set to `false` for development, this will also create sourcemaps.
* *The preprocessor tasks perform structural CSS optimization as well as media-query-merging.*
* If you do not want or need a preprocessor, the easiest way to go about it is by using the LESS preprocessor and just writing 'vanilla' CSS in files with the `.less` extension.

### `js` (object) – Script File Processing

* The `loader` option should be kept at `simple` for now (We're still working on Browserify, Require.js and React.js support).
* The `bundle` array should contain paths to all script files you want to bundle up. You can use path aliases here.
* `minify` controls minification as well as sourcemap generation, just like the CSS option.

### `images` (object) - Image Optimizations

* Configure the optimization level with the `images.optimize` option:
    * `false` turns off the optimizer
    * `"low"`, `"medium"`, `"high"` and `"absurd"` turn on and control the optimization level.

### `livereload` (array)

* You can pass an array of globbing expressions that are then watched for changes, triggering a livereload when they have.
* You can use path aliases here.
* Get the livereload extension for Google Chrome [at the Chrome Web Store](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en)

### `copy` (array)

Use the copy configuration to copy files (e.g. fonts or other media) to your build directory.

* The `copy` option is an object containing key-value-pairs.
* Each pair should be in the form of `"<from>": "<to>"`, where `<from>` is the globbing expression used to select the files you want to copy, and `<to>` is the destination. 
* You can use path aliases here.
* You don't need to manually copy CSS, JS or image files. Those are taken care of by the other tasks.

### `archive` (object)

_Coming soon._

### `bower` (object)

_Coming soon._

---

## Recommendations

### jQuery

* [You might not need jQuery](http://youmightnotneedjquery.com/)
* [Zepto.js](http://zeptojs.com/) might also be a good idea.
* Libraries like [Cash](https://github.com/kenwheeler/cash) probably cover most of the things you'd use jQuery for.

### Installing new assets

* Try to use Bower to install new asset dependencies. `cd` into the `boilerplate` directory and use `bower search` and `bower install`.
* If the package you need is not available through Bower, we have a convention for that: Place whatever you need from your package in `resources/{js,img,fonts,sass}/vendor/<package-name>`.
* Adjust the `js.bundle` option in `project.json` if you added script files and are using the `simple` loader.

---

## Available commands/tasks

This is just a quick overview of what the boilerplate can do for you. We recommend sticking to the default task by simpl running `gulp` with no arguments.

* `archive`: Zips files from your manifest list
* `bower`: Takes care of all your Bower package asset publishing
* `copy`: Simple copy task which reads globbing expressions from project.json
* `css`: Dynamic task switch, runs the configured CSS preprocessor (*less* or *sass*, running `less` or `sass` respectively)
* `images`: Copies theme and content images and then optimizes them
* `js`: Dynamic task switch, runs the configured JS loader (*simple* or *browserify*, running `scripts` and `browserify` respectively)
* `watch`: Starts several watchers to keep an eye on those files of y'all

---

## Changelog

### Version 2.2.0 *(08-06-2015)*

* This project is now available under the GNU General Public License v3.0 only (GPL-3.0).
* New: You can now use SASS/SCSS. Set `css.preprocessor` to `sass` and adjust the entrypoint.
* New: You can now use the `simple` option for `js.loader`. Specify all files you want to bundle in the `js.bundle` array (you can use path aliases).
* New: You can now define copy operations in the `copy` object. See the Usage chapter for more information.
* New: You can now control the image optimization level, and turn it off completely.
* Improved: Image optimization for files in the `{content}` path are now in-place.
* Improved: Error handling is now transparently handled with a plumbed `gulp.src()` in `helper.js`. Tasks now continue even if errors occur.
* Improved: The directory structure of the boilerplate itself as well as the recommendation for your project has changed. See the Installation chapter for more information.
* Fixed: The `livereload` task now correctly live-reloads the specified files.
* Freezed: The `browserify` tasks have been put on ice. Use the `simple` option for `js.loader` for now while we're working on support for AMD/Common.JS module loading.

### Version 2.1.0 *(06-29-2015)*

* New: The project helper now contains three new methods to access the project.json file's values:
    * `Helper.has(configOption)` accepts a string with object properties in dot notation, and returns whether or not the project.json has that setting configured (Example: `helper.has('archive.file.prefix')` will return `true` if you have archive.file.prefix set in project.json). _From now on we will refer to configuration options in this notation in this document._
    * `Helper.read(configOption)` is the same as `Helper.has()`, but it returns the full value (or `undefined` if `Helper.has()` return `false`)
    * `Helper.get(configOption, defaultValue)` is the same as `Helper.read()`, but it accepts a `defaultValue` as the second parameter, which is used if `configOption` is not readable
* New: `gulp archive`, `gulp archive:cleanup`, `gulp archive:build`
    * Reads the configuration from `project.archive`
* Fixed: `gulp watch` now correctly depends on all availlable watchers.
* Improved: Error handling in gulp tasks at the highest level now looks somewhat nicer.

### Version 2.0.0 *(06-28-2015)*

* First stable release of the new boilerplate! Have a look, documentation is in the works. 

### Version 1.0.0

* Legacy release of the old boilerplate code.