var requireReload = require('require-reload'),
    style = require('gulp-util').colors,
    helper = require('./helper'),
    validator = require('tv4'),
    jsonSchema = requireReload('./project-schema.json'),
    jsonInstance = requireReload('../../../project.json');

// Validate the project.json file against our schema
var results = validator.validateMultiple(jsonInstance, jsonSchema);

if (!results.valid) {
    helper.info('' + style.underline('project.json') + ' contains at least the following errors:', true);

    // If there are errors, display them nicely
    results.errors.forEach(function (error) {
        helper.warning(style.red('✖') + ' ' + error.message);
    });

    // And finally error out
    throw new Error("Please fix the problems in your project.json before running gulp again.");
} else {
    helper.success('✓ Building ' + helper.get('title'));
}

module.exports = {};