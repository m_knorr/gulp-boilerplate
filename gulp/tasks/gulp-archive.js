var gulp = require('gulp'),
    del = require('del'),
    tap = require('gulp-tap'),
    zip = require('gulp-zip'),
    moment = require('moment'),
    helper = require('../support/helper');

/**
 * Task "archive"
 */
gulp.task('archive', ['archive:build'], function (callback) {
    return callback();
});

/**
 * Task "archive:cleanup"
 *
 * Cleans up the archive file.
 */
gulp.task('archive:cleanup', function (callback) {
    var archivePath = helper.replacePaths(helper.get('archive.file.destination', helper.dir('archive')));
    var extension = helper.get('archive.file.extension', '.zip');
    var archives = archivePath + '/*' + extension;

    helper.info('Cleaning up archive files in "' + archivePath + '"');

    del([archives], {
        force: true
    }, function (error, paths) {
        paths.forEach(function (path) {
            helper.comment("Deleted " + path + "!");
        });
        callback();
    });
});

/**
 * Task "archive:build"
 *
 *
 */
gulp.task('archive:build', function () {
    helper.info('Building archive from manifest ...');
    if (!helper.has('archive.manifest')) {
        helper.comment('Cannot build archive without a manifest!');
        return;
    }

    var now = moment();
    var extension = helper.get('archive.file.extension', '.zip');
    var filename = helper.get('archive.file.prefix', 'archive.') +
        now.format('YYYY-MM-DD-HH-mm-ss') +
        helper.get('archive.file.suffix', helper.get('archive.compression', true) ? '.compressed' : '') + extension;

    // @todo add support for destination inside the archive
    // (Do we have to create a temporary directory for this?)
    return helper.src(helper.get('archive.manifest'))
        .pipe(tap(function (file) {
            helper.comment("Archiving " + file.path + " ...");
        }))
        .pipe(zip(filename, {
            compress: helper.get('archive.compression', true)
        }))
        .pipe(gulp.dest(helper.replacePaths(helper.get(
            'archive.file.destination',
            helper.dir('archive')
        ))))
        .on('end', function () {
            helper.success("Finished compiling archive sources!");
        });
});