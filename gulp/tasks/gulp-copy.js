var gulp = require('gulp'),
    tap = require('gulp-tap'),
    merge = require('merge-stream'),
    helper = require('../support/helper');

/**
 * Task "copy"
 */
gulp.task('copy', ['copy:build'], function (callback) {
    callback();
});

/**
 * Task "copy:build"
 */
gulp.task('copy:build', function (callback) {
    helper.info('Publishing project assets ...');

    // Check if we have copy tasks configured
    if (!helper.has('copy')) {
        helper.comment('Nothing to copy.');
        return;
    }

    var stream;
    var paths = helper.get('copy');

    var globs = Object.getOwnPropertyNames(paths);

    globs.forEach(function (glob) {
        var destination = helper.replacePaths(paths[glob]);

        var newStream = helper.src(glob)
            .pipe(tap(function (file) {
                helper.comment("Copying " + file.relative + " to " + destination + " ...");
            }))
            .pipe(gulp.dest(destination));

        stream = stream ?
            merge(
                stream,
                newStream
            ) : newStream;
    });

    // No copy no fun.
    if(stream) {
        return stream;
    } else {
        callback();
    }
});