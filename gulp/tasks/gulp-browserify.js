var gulp = require('gulp'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    uglify = require('gulp-uglify'),
    debowerify = require('debowerify'),
    deamdify = require('deamdify'),
    streamify = require('gulp-streamify'),
    helper = require('../support/helper');

gulp.task('browserify', ['browserify:bundle'], function (callback) {
    callback();
});

/**
 * Task "browserify:bundle"
 */
gulp.task('browserify:bundle', function (callback) {
    var transforms = [debowerify, deamdify];

    var bundler = browserify({
        entries: [
            helper.dir('{theme}/js/main.js')
        ],
        debug: helper.get('js.minify', true), // "minify" will disable this
        cache: {},
        packageCache: {},
        fullPaths: true,
        transform: transforms
    });

    var bundle = bundler.bundle();

    var bundleAll = function () {
        bundle = bundler
            .bundle()
            .pipe(source('main.js'));

        if(helper.get('js.minify') === true) {
            bundle = bundle.pipe(streamify(uglify()));
        }

        bundle
            .pipe(gulp.dest(helper.dir('{build}/js')));

        return bundle;
    };

    bundleAll();
    return callback();
});