var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    helper = require('../support/helper'),
    combineMediaQueries = require('gulp-combine-media-queries'),
    minify = require('gulp-minify-css');

gulp.task('sass', ['sass:build'], function (callback) {
    callback();
});

gulp.task('sass:build', function () {

    var sassFiles = helper.src(helper.get('css.entrypoint'))
        // We always have to pipe this into the sourcemaps plugin,
        // even when minification is turned on, because gulp-sass
        // or one of its dependencies breaks the SASS compiler
        // otherwise.
        .pipe(sourcemaps.init())
        .pipe(sass({
            // Set the default, non-minified output style
            outputStyle: 'compact',
            // For good measure
            includePaths: [helper.dir('{theme}/sass/')],
            // Does this work at all?
            disableWarnings: true
        }));

    if (helper.get('css.autoprefix') == true) {
        helper.comment("Autoprefixing SASS sources when compiling ...");
        // Let's be greedy
        sassFiles.pipe(autoprefixer('last 6 versions'));
    }

    if (helper.get('css.minify') == false) {
        helper.comment("Generating sourcemaps ...");
        // We'll only write the sourcemap if minification is turned off.
        // @todo Add reasoning behind this to documentation.
        // @todo Write documentation first.
        sassFiles.pipe(sourcemaps.write());
    } else {
        helper.comment("Minifying CSS after compiling ...");
        // @todo Evaluate whether this is actually needed in combination with gulp-minify-css
        sassFiles.pipe(combineMediaQueries({log: true}));
        sassFiles.pipe(minify());
    }

    return sassFiles.pipe(gulp.dest(helper.dir('{build}/css')));

});